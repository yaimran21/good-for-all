<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	{{-- Styles --}}
	@include('_includes.head')
	{{-- Custom Styles --}}
	@yield('styles')

</head>
<body>
	<div id="app">  
		@include('_includes.nav.nav_main')
		@yield('content')
	</div>

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}"></script>
		{{-- For custom script --}}
	@yield('scripts')
</body>
</html>
