<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@section('title','management')
<head>
	{{-- Styles --}}
	@include('_includes.head')
	{{-- Custom Styles --}}
	@yield('styles')
</head>
<body>
	@include('_includes.nav.nav_main')
	@include('_includes.nav.nav_manage')
	<div class="management-area" id="app">  

		@yield('content')
	</div>
</body>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
{{-- For custom script --}}
@yield('scripts')
</html>
