@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-one-third is-offset-one-third m-t-50">
        <div class="card">
            {{-- Start card content --}}
            <div class="card-content">
                <h1 class="title">Join Our Community</h1>
                <hr/>
                <form action="{{ route('register') }}" method="POST" role="form">
                   {{csrf_field()}}
                   <div class="field">
                       <label for="name" class="label">Name:</label>
                       <p class="control">
                           <input class="input {{$errors->has('name') ? 'is-danger':''}}" type="name" name="name" id="name" placeholder="your name..">
                       </p>
                       @if ($errors->has('name'))
                       <p class="help is-danger">{{ $errors->first('name') }}</p>
                       @endif
                   </div>

                   <div class="field">
                       <label for="email" class="label">Email Address:</label>
                       <p class="control">
                           <input class="input {{$errors->has('email') ? 'is-danger':''}}" type="email" name="email" id="email" placeholder="Ex: your@email.com">
                       </p>
                       @if ($errors->has('email'))
                       <p class="help is-danger">{{ $errors->first('email') }}</p>
                       @endif
                   </div>

                 <div class="columns">
                 <div class="column">
                       <div class="field">
                       <label for="password" class="label">Password:</label>
                       <p class="control">
                           <input class="input {{$errors->has('password') ? 'is-danger':''}}" type="password" name="password" id="password" placeholder="password..">
                       </p>
                       @if ($errors->has('password'))
                       <p class="help is-danger">{{ $errors->first('password') }}</p>
                       @endif
                   </div>  
                   </div>  
                 <div class="column">
                   <div class="field">
                       <label for="password_confirmation" class="label">Confirm Password:</label>
                       <p class="control">
                           <input class="input {{$errors->has('password') ? 'is-danger':''}}" type="password" name="password_confirmation" id="password_confirmation" placeholder="confirm password..">
                       </p>
                       @if ($errors->has('password_confirmation'))
                       <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
                       @endif
                   </div>
                   </div>
                 </div>
                   <button class="button is-success is-outlined is-fullwidth m-t-20">Register</button>
               </form>
               <h5 class="has-text-centered m-t-10">
                <a class="is-muted" href="{{ route('login') }}">Already have an account?</a>
            </h5> 
        </div>
        {{-- End card content --}}
    </div>
    {{-- End card --}}
    
</div>
</div>
@endsection