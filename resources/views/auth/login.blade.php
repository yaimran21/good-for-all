@extends('layouts.app')

@section('content')
<div class="columns">
    <div class="column is-one-third is-offset-one-third m-t-50">
        <div class="card">
            {{-- Start card content --}}
            <div class="card-content">
                <h1 class="title">Sign In</h1>
                <hr/>
                <form action="{{ route('login') }}" method="POST" role="form">
                   {{csrf_field()}}
                   
                   <div class="field">
                       <label for="email" class="label">Email Address:</label>
                       <p class="control">
                           <input class="input {{$errors->has('email') ? 'is-danger':''}}" type="email" name="email" id="email" placeholder="Ex-example@email.com" value="{{old('email')}}">
                       </p>
                            @if ($errors->has('email'))
                                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                           @endif
                   </div>

                   <div class="field">
                       <label for="password" class="label">Password:</label>
                       <p class="control">
                           <input class="input {{$errors->has('password') ? 'is-danger':''}}" type="password" name="password" id="password" placeholder="*************">
                       </p>
                          @if ($errors->has('password'))
                                    <p class="help is-danger">{{ $errors->first('password') }}</p>
                           @endif
                   </div>
                   <b-checkbox name="remember" class="m-to-10">Remember Me</b-checkbox>
                   <button class="button is-success is-outlined is-fullwidth m-t-20">Sign In</button>

           </form>
           <h5 class="has-text-left m-t-10">
            <a class="is-muted" href="{{ route('password.request') }}">Forgot Your Password?</a>
        </h5> 
          <h5 class="has-text-right m-t-10">
            <a class="is-muted" href="{{ route('register') }}">Create an account?</a>
        </h5> 
    </div>
    {{-- End card content --}}
    </div>
    {{-- End card --}}
    
</div>
</div>
@endsection
