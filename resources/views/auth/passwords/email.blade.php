@extends('layouts.app')
@section('content')
         @if (session('status'))
                 <div class="notification is-success">
                   {{ session('status') }}
               </div>
         @endif
<div class="columns">
    <div class="column is-one-third is-offset-one-third m-t-50">
        <div class="card">
            {{-- Start card content --}}
            <div class="card-content">
                <h1 class="title">Reset Password</h1>
                <form action="{{ route('password.email') }}" method="POST" role="form">
                   {{csrf_field()}}
                   <div class="field">
                       <label for="email" class="label">Email Address:</label>
                       <p class="control">
                           <input class="input {{$errors->has('email') ? 'is-danger':''}}" type="email" name="email" id="email" placeholder="Ex-arafat@gmail.com" value="{{old('email')}}">
                       </p>
                            @if ($errors->has('email'))
                                    <p class="help is-danger">{{ $errors->first('email') }}</p>
                           @endif
                   </div>
                   <button class="button is-success is-outlined is-fullwidth m-t-20">Send Password Reset Link</button>
           </form>
           <h5 class="has-text-centered m-t-10">
            <a class="is-muted" href="{{ route('login') }}"><i class="fa fa-caret-left"></i> Back to Login</a>
        </h5> 
    </div>
    {{-- End card content --}}
    </div>
    {{-- End card --}}
    
</div>
</div>
@endsection