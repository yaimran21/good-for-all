  @extends('layouts.manage')
  @section('content')
  <div class="flex-container">
    <div class="card">
      <div class="card-content">
        <div class="columns m-t-10">
          <div class="column">
            <h1 class="title">Update Users Data</h1>
          </div>
        </div>
      </div>
    </div>
    <hr/>
    {{-- Start Form --}}
    <div class="card">
      <div class="card-content">
        <div class="columns">
          <div class="column">
            <form action="{{ route('users.update',$user->id) }}" method="POST">
              {{method_field('PUT')}}
              {{csrf_field()}}

              <div class="field">
                <label for="name" class="label">Name</label>
                <p class="control">
                  <input class="input" type="text" name="name" id="name" value="{{$user->name}}" placeholder="Enter Name">
                </p>
              </div>

              <div class="field">
                <label for="email" class="label">Email</label>
                <p class="control">
                  <input class="input" type="email" name="email" id="email" value="{{$user->email}}" placeholder="Enter Email">
                </p>
              </div>
              {{-- Radio --}}
                 <div class="field">
                <label for="password" class="label">Password</label>
                <b-radio-group v-model="password_options">
                  <div class="field">
                    <b-radio name="password_options" value="keep">Do Not Change Password</b-radio>
                  </div>
                  <div class="field">
                    <b-radio name="password_options" value="auto">Auto-Generate New Password</b-radio>
                  </div>
                  <div class="field">
                    <b-radio name="password_options" value="manual">Manually Set New Password</b-radio>
                    <p class="control">
                      <input type="text" class="input m-t-10" name="password" id="password" v-if="password_options == 'manual'" placeholder="Manually give a password to this user">
                    </p>
                  </div>
                </b-radio-group>
              </div>
              {{-- Rario End --}}

           
              <button class="button is-primary">Update User</button>

            </form>
              {{-- End form --}}

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
<script>
  var app = new Vue({
    el:'#app',
    data:{
      password_options:'keep'
    }
  });
</script>
@endsection
