@extends('layouts.manage')
@section('content')
<div class="flex-container">
	<div class="card">
		<div class="card-content">
			<div class="columns m-t-10">
				<div class="column">
			<h1 class="title">Manage Users</h1>
		</div>
		<div class="column">
			<a href="{{ route('users.create') }}" class="button is-primary is-pulled-right">Create New User<i class="fa fa-user-create"></i></a>
		</div>
		</div>
		</div>
	</div>
	<hr/>
	<div class="card">
		<div class="card-content">
			<table class="table is-narrow">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Email</th>
				<th>Date Created</th>
				<th>action</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
				<tr>
					<td>{{$user->id}}</td>
					<td>{{$user->name}}</td>
					<td>{{$user->email}}</td>
					<td>{{$user->created_at->toFormattedDateString()}}</td>
					<td><a class="button is-outlined" href="{{ route('users.edit',$user->id) }}"><i class="fa fa-edit">	</i>
					</a>
					<a class="button is-outlined" href="{{ route('users.show',$user->id) }}"><i class="fa fa-eye"></i></a>
					</td>
					
				</tr>
			@endforeach
		</tbody>
	</table>
		</div>
	</div>
	{{-- Responsible for pagination --}}
	{{ $users->links() }}
</div>

@endsection