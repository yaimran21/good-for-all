@extends('layouts.manage')
@section('content')
<div class="flex-container">
	<div class="card">
		<div class="card-content">
			<div class="columns m-t-10">
				<div class="column">
					<h1 class="title">View User Details</h1>
				</div>
			</div>
		</div>
	</div>
	<hr/>

		<div class="card">
			<div class="card-content">
				<div class="columns">
				<div class="column">
					<label for="name" class="level">Name</label>
					<pre>{{$user->name}}</pre>
				</div>
				<div class="column">
					<label for="email" class="level">Email</label>
					<pre>{{$user->email}}</pre>
				</div>
					<div class="column">
					<label for="action" class="level">Action</label>
					<a href="{{ route('users.edit',$user->id) }}" class="button is-pulled-right"><i class="fa fa-edit"></i></a>
				
				</div>
			</div>
		</div>
	</div>
</div>
@endsection