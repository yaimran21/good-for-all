@extends('layouts.manage')
@section('content')
<div class="flex-container">
	<div class="card">
		<div class="card-content">
			<div class="columns m-t-10">
				<div class="column">
					<h1 class="title">Create New Users</h1>
				</div>
			</div>
		</div>
	</div>
	<hr/>
	{{-- Start Form --}}
	<div class="card">
		<div class="card-content">
			<div class="columns">
				<div class="column">
					<form action="{{ route('users.store') }}" method="POST">
						{{csrf_field()}}
						<div class="field">
							<label for="name" class="label">Name</label>
							<p class="control">
								<input class="input" type="text" name="name" id="name" placeholder="Enter Name">
							</p>
						</div>

						<div class="field">
							<label for="email" class="label">Email</label>
							<p class="control">
								<input class="input" type="email" name="email" id="email" placeholder="Enter Email">
							</p>
						</div>


						<div class="field">
							<label for="password" class="label">Password</label>
							{{-- <p class="control"> --}}
								{{-- <input class="input" type="password" name="password" id="password" v-if="!auto_password" placeholder="Manually Enter Password"> --}}
								<b-checkbox class="m-t-10" v-model="auto_password" name="auto_generate" :checked="true">Auto Generate Password</b-checkbox>

							{{-- </p> --}}
						</div>
						<button class="button is-success">Create User</button>

					</form>
				</div>
			</div>
		</div>
	</div>
	{{-- End form --}}
</div>
</div>

@endsection
@section('scripts')
<script>
	var app =new Vue({
		el:#app,
		data:{
			auto_password:true
		}
	});
</script>
@endsection