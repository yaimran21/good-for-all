<div class="side-menu">
	<aside class="menu m-t-30 m-l-10">
		<p class="menu-level">
			GENERAL
		</p>
		<ul class="menu-list">
			<li><a href="{{ route('manage.dashboard') }}">Dashboard</a></li>
		</ul>
		<p class="menu-level">
			ADMINITRATION
		</p>
		<ul class="menu-list">
			<li><a href="{{ route('users.index') }}">Manage User</a></li>
			<li><a href="#">Roles &amp; Permission</a></li>
		</ul>
	</aside>
</div>