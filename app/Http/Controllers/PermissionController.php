<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Permissions = Permissions::all();
        return view('manage.permissions.index')->withPermission($permissions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->permissions_type == 'basic') {
            $this->validate($request,[
                'display_name' => 'required|max:255',
                'name' => 'required|max:255|alphadash|unique:permissions,name',
                'description' => 'sometimes|max:255'
                ]);

            $permission = new Permissions();
            $permission->name = $request->name;
            $permission->display_name = $request->display_name;
            $permission->description = $request->description;
            $permission->save();

            Session::flash('success', 'Permissions has been successfully added');
            return redirect()->route('permission.index');
        }
        elseif ($request->permissions_type == 'crud') {
            $this->validate([
                'resource' => 'required|min:3|max:100|alpha'
                ]);
            $crud = explode(',', $crud_selected);
            if (count($crud)>0) {
                foreach ($crud as $x) {
                    $slug = strtolower($x) . '_' .$request->resource;
                    $display_name = ucwords($x ." " .$request->resource);
                    $description = "Allows a user to" . strtoupper($x) . ' a ' . ucwords($request->resource);

                    $permission = new Permissions();
                    $permission->name = $slug;
                    $permission->display_name = $display_name;
                    $permission->description = $description;
                    $permission->save();
                }
                Session::flash('success', 'Permissions were all successfully added');
                return redirect()->rout('permission.index');
            }
        }
        else{
            return redirect()->route('permission.create')->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permissions::findORFail($id);
        return view('manage.permission.show')->withPermission($permission);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permissions::findORFail($id);
        return view('manage.permission.edit')->withPermission($permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'display_name' => 'required|max:255',
            'description' => 'sometimes|max:255'
            ]);
        $permission = Permissions::findORFail($id);
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();
        Session::flash('success', 'Update the'. $permission->display_name . 'permission');
        return redirect()->route('permission.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
